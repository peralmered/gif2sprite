#!/usr/bin/env python
# -*- coding: utf-8 -*-

strVersion="0.1"
strProgramHeaderShort="gif2sprite.py v"+strVersion
strProgramHeader="---[ "+strProgramHeaderShort+" ]------------------------------------------------"
strProgramDescription="[ converts gif files to sprites ]"

"""

===============================================================================================
===============================================================================================
== ToDo

https://bitbucket.org/peralmered/gif2sprite/issues?status=new&status=open

== ToDo
===============================================================================================
===============================================================================================
== Thoughts


== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True
if boolAllowScreenOutput: print strProgramHeader

boolDebug=True
boolDebugPrinting=False

################################################################################################
################################################################################################
## Defines


## Defines
################################################################################################
################################################################################################
## Constants


###############################################################################
## In xia.py

# COLOR_FORMAT_STE="ste"
# COLOR_FORMAT_TT="tt"
# COLOR_FORMAT_FALCON="falcon"
# COLOR_FORMAT_NUMERIC_STE=0
# COLOR_FORMAT_NUMERIC_TT=1
# COLOR_FORMAT_NUMERIC_FALCON=2

## In xia.py
###############################################################################


#------------------------------------------------------------------------------


## Constants
################################################################################################
################################################################################################
## Globals


numImageWidth=None
numImageHeight=None


## Globals
################################################################################################
################################################################################################
## Functions


def DumpPal():
  global numBitplanes
  Out("  Palette:\n    ")
  numCol=1
  for this in arrAtariPal:
    if strColorFormat==xia.COLOR_FORMAT_FALCON:
      strCol=xia.MakeDevpacLongword(this)
    else:
      strCol=xia.MakeDevpacWord(this)
    Out(strCol)
    if numCol%8:
      if numCol!=len(arrAtariPal):
        Out(", ")
    else:
      Out("\n    ")
    numCol+=1;
    if numCol>pow(2,numBitplanes):
      break
  Out("\n")


#------------------------------------------------------------------------------


def MakePlanes(arr16):
  # We start by converting as if it was an 8-bpl image
  arrPlanes=8*[0]
  numBitMask=32768
  for thisPixel in arr16:
    strPixel=xia.GetStringByte(thisPixel)
    for i in range(len(strPixel)):
      thisChar=strPixel[i]
      if thisChar=="1":
        arrPlanes[7-i]+=numBitMask
    numBitMask>>=1
  arrOut=[]
  # Remove unused planes
  for i in range(numBitplanes):
    arrOut.append(arrPlanes[i])
  return arrOut


#------------------------------------------------------------------------------


def OverWriteMask(arrM, arrDest, numWidth, numHeight, xoffs, yoffs):
  #print "    "+str(xoffs)+", "+str(yoffs)
  for y in range(numHeight):
    for x in range(numWidth):
      numPos=(y*numWidth)+x
      numOffsetPos=((y+yoffs)*numWidth)+(x+xoffs)
      if (x+xoffs)<0 or (x+xoffs)>numWidth or (y+yoffs)<0 or (y+yoffs)>numHeight:
        #print "x: "+str(x)+", xoffs: "+str(xoffs)+", y: "+str(y)+", yoffs: "+str(yoffs)
        try:
          bit=arrM[numOffsetPos]
        except:
          bit=0
        if bit==0:
          #print "xy: ("+str(x)+", "+str(y)+")"
          arrResult={}
          arrResult["success"]=False
          return arrResult
      else:
#        if arrM[numOffsetPos]==1:
#          arrDest[numPos]|=1
        if arrM[numPos]==0:
          arrDest[numOffsetPos]=0
  arrResult={}
  arrResult["success"]=True
  arrResult["mask"]=arrDest
  return arrResult


#------------------------------------------------------------------------------


def PreviewImage(arrImage):
  global numImageWidth
  global numImageHeight
  print "Image:"
  n=0
  for y in range(numImageHeight):
    strLine=""
    for x in range(numImageWidth):
      strLine+=str(arrImage[n])+" "
      n+=1
    print strLine
  print


#------------------------------------------------------------------------------


def PreviewMask(arrMask):
  global numImageWidth
  global numImageHeight
  print "Mask:"
  n=0
  for y in range(numImageHeight):
    strLine=""
    for x in range(numImageWidth):
      strLine+=str(arrMask[n])+" "
      n+=1
    print strLine
  print


#------------------------------------------------------------------------------


def PreviewWordImage(arrImage):
  global numImageWidth
  global numImageHeight
  print "\nImage:"
  n=0
  for y in range(numImageHeight):
    strLine=""
    for x in range(numImageWidth/16):
      strWord=bin(arrImage[n])[2:].zfill(16)
      for c in strWord:
        strLine+=str(c)+" "
      n+=1
    print strLine
  print


#------------------------------------------------------------------------------


def PreviewWordMask(arrMask):
  global numImageWidth
  global numImageHeight
  print "\nMask:"
  n=0
  for y in range(numImageHeight):
    strLine=""
    for x in range(numImageWidth/16):
      strWord=bin(arrMask[n])[2:].zfill(16)
      for c in strWord:
        strLine+=str(c)+" "
      n+=1
    print strLine
  print


#------------------------------------------------------------------------------


def ConvertImagetoBitplanesAndMask(arrImage, arrMask, numColorMask):
#  Out("  Sprite size: "+str(numImageWidth)+" x "+str(numImageHeight)+"\n")
#  print "array: "+str(len(arrImage))
#  print "bitplanes: "+str(numSpriteBitplanes)

  boolLocalDebug=False  # True  # False  # True

  if boolLocalDebug:
    PreviewImage(arrImage)

    if boolMaskFileGiven:
      PreviewMask(arrMask)

  numWordWidth=numImageWidth/16
  arrSpriteData=[]
  arrMaskData=[]
  numPos=0
  for y in range(numImageHeight):
    for x in range(numWordWidth):
      arrThese16=arrImage[numPos:numPos+16]
      arrPlanes=MakePlanes(arrThese16)
      if boolColorMask:
        thisMask=[]
        for c in arrThese16:
          if c==numColorMask:
            thisMask.append(1)
          else:
            thisMask.append(0)
        arrMaskData.extend(thisMask)
      for thisPlane in arrPlanes:
        arrSpriteData.append(thisPlane)
      numPos+=16


  if boolLocalDebug:
    if not boolMaskFileGiven:
      print "* arrMaskData:"
      n=0
      for y in range(numImageHeight):
        strLine=""
        for x in range(numImageWidth):
          strLine+=str(arrMaskData[n])+" "
          n+=1
        print strLine
      print
      print "#"*78
      print


  if boolFeather:
    arrDestMask=copy.deepcopy(arrMaskData)
    arrOffsets=[]
    arrOffsets.append([-1, -1])
    arrOffsets.append([0, -1])
    arrOffsets.append([1, -1])
    arrOffsets.append([-1, 0])
    #arrOffsets.append([0, 0])
    arrOffsets.append([1, 0])
    arrOffsets.append([-1, 1])
    arrOffsets.append([0, 1])
    arrOffsets.append([1, 1])
    for i,o in enumerate(arrOffsets):
      if boolLocalDebug: print "---[ "+str(i).zfill(4)+" ]-------------------------------"
      arrResult=OverWriteMask(arrMaskData, arrDestMask, numImageWidth, numImageHeight, o[0], o[1])
      if arrResult["success"]==False:
        Fatal("Can't use --feather - there aren't enough \"free\" pixels surrounding the image!")
      else:
        arrDestMask=copy.deepcopy(arrResult["mask"])
        n=0
        for y in range(numImageHeight):
          strLine=""
          for x in range(numImageWidth):
            strLine+=str(arrDestMask[n])+" "
            n+=1
          if boolLocalDebug: print str(y).zfill(2)+"    "+strLine
        if boolLocalDebug: print
    arrMaskData=copy.deepcopy(arrDestMask)

  if boolLocalDebug:
    print "numColorMask: "+str(numColorMask)

  # Convert arrMaskData to binary
  if boolMaskFileGiven:
    arrTemp=copy.deepcopy(arrMask)
    numColorMask=0
  else:
    arrTemp=copy.deepcopy(arrMaskData)
  arrMaskData=[]
  numPos=0
  for y in range(numImageHeight):
    for x in range(numWordWidth):
      arrThese16=arrTemp[numPos:numPos+16]
      numBitMask=pow(2,15)
      #print "arrThese16: "+str(arrThese16)
      thisMask=0
      for p in arrThese16:
        if p!=numColorMask:
          thisMask+=numBitMask
        numBitMask>>=1
      arrMaskData.append(thisMask)
      numPos+=16

  if boolLocalDebug:
    if not boolMaskFileGiven:
      print "*** arrMaskData:"  #  "+str(arrMaskData)
      for m in arrMaskData:
        strM=bin(m)[2:].zfill(16)
        strOut=""
        for c in strM:
          strOut+=c+" "
        print strOut
      print

  if boolLocalDebug:
    print "-"*78
    print "arrSpriteData: "+str(arrSpriteData)
    print
    print "arrMaskData: "+str(arrMaskData)
    print

  arrResult={}
  arrResult["sprite"]=arrSpriteData
  arrResult["mask"]=arrMaskData
  return arrResult


#------------------------------------------------------------------------------


def TurnWordsIntoBytes(arrData):
  arrOut=[]
  for thisWord in arrData:
    numHighByte=(thisWord>>8)&0xff
    numLowByte=thisWord&0xff
    arrOut.append(numHighByte)
    arrOut.append(numLowByte)
  return arrOut


## Functions
################################################################################################
################################################################################################
## Libraries


if boolDebug:
  print "Importing libraries...",
import sys
import os
import subprocess
from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy
import random
import math

sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out
#
#import pdb

if boolDebug:
  print "done\n"


## Libraries
################################################################################################
################################################################################################
## Test code


if 1==2:
  exit(0)


## Test code
################################################################################################
################################################################################################
## Handle input parameters


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="sprite image to load",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-m", "--maskfile",
                       dest="maskfile",
                       help="sprite mask to load",
                       metavar="FILE")
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="file stub to save to",
                       metavar="FILE")
objParser.add_argument("-mg", "--savemaskgif",
                       dest="savemaskgif",
                       help="save mask as .gif (default is off)",
                       action="store_const",
                       const=True)
objParser.add_argument("-c", "--color-format",
                       dest="colorformat",
                       help="palette color format, 'ste', 'tt' or 'falcon' (default is 'ste')",
                       metavar="STRING",
                       choices=(xia.COLOR_FORMAT_STE, xia.COLOR_FORMAT_TT, xia.COLOR_FORMAT_FALCON),
                       required=False)
objParser.add_argument("-b", "--bitplanes",
                       dest="bitplanes",
                       help="number of bitplanes of destination screen (1-8, default is 4)",
                       metavar="NUMBER",
                       required=False)
objParser.add_argument("-am", "--automask",
                       dest="automask",
                       help="auto mask (color 0 becomes mask) (default is off)",
                       action="store_const",
                       const=True)
objParser.add_argument("-cm", "--colormask",
                       dest="colormask",
                       help="color to use for mask",
                       metavar="NUMBER",
                       required=False)
objParser.add_argument("-f", "--feather",
                       dest="feather",
                       help="feather mask (1 pixel border outside sprite data, increases sprite size if needed) (default is off)",
                       action="store_const",
                       const=True)
objParser.add_argument("-ib", "--interleavebitplanes",
                       dest="interleavebitplanes",
                       help="interleave bitplanes in sprite data (default is off)",
                       action="store_const",
                       const=True)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
objParser.add_argument("-q", "--quiet",
                       dest="quiet",
                       help="no output during processing, except errors (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolMaskFileGiven=False
strMaskFile=""
boolOutFileGiven=False
strOutFile=""

boolAutoMask=False
boolColorMask=False
numColorMask=-1
boolFeather=False
boolInterleaveBitplanes=False
boolSaveMaskAsGif=False

boolVerboseMode=False
boolQuietMode=False
strColorFormat=xia.COLOR_FORMAT_STE
numBitplanes=4


numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  if key=="maskfile" and strValue!="None":
    boolMaskFileGiven=True
    strMaskFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="savemaskgif" and strValue!="None":
    boolSaveMaskAsGif=True
  elif key=="colorformat" and strValue!="None":
    strColorFormat=strValue
  elif key=="bitplanes" and strValue!="None":
    numBitplanes=int(strValue)
  elif key=="automask" and strValue!="None":
    boolAutoMask=True
    boolColorMask=True
    numColorMask=0
  elif key=="colormask" and strValue!="None":
    boolColorMask=True
    numColorMask=int(strValue)
  elif key=="feather" and strValue!="None":
    boolFeather=True
  elif key=="interleavebitplanes" and strValue!="None":
    boolInterleaveBitplanes=True

  elif key=="verbose" and strValue!="None":
    boolVerboseMode=xia.StrToBool(strValue)
  elif key=="quiet" and strValue!="None":
    boolQuietMode=xia.StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)


# Test number of bitplanes
if numBitplanes not in [1, 2, 3, 4, 5, 6, 7, 8]:
  Fatal("Number of bitplanes must be 1-8, not "+str(numBitplanes)+"!")


# Adjust combinations of verbose and quiet mode
if (boolVerboseMode & boolQuietMode):
  Notice("Both verbose and quiet mode sent as parameters, verbose mode chosen.\n")
  boolQuietMode=False


if boolOutFileGiven==False:
  strOutFile=strInFile


# Test infile
numResult=xia.CheckFileExistsAndReadable(strInFile)
if numResult!=0:
  if numResult&xia.FILE_EXISTS_FAIL:
    Fatal("Can't find file \""+strInFile+"\"!")
  if numResult&xia.FILE_READABLE_FAIL:
    Fatal("Can't read file \""+strInFile+"\"!")


if boolMaskFileGiven:
  # Test maskfile
  numResult=xia.CheckFileExistsAndReadable(strMaskFile)
  if numResult!=0:
    if numResult&xia.FILE_EXISTS_FAIL:
      Fatal("Can't find file \""+strMaskFile+"\"!")
    if numResult&xia.FILE_READABLE_FAIL:
      Fatal("Can't read file \""+strMaskFile+"\"!")


## Handle input parameters
################################################################################################
################################################################################################
## Reading input data


Out("Reading sprite file \""+strInFile+"\"... ")
arrInFile=xia.GetBinaryFile(strInFile)
try:
  imgIn=Image.open(strInFile)
except IOError:
  Fatal("\""+strInFile+"\" isn't a valid image file!")
Out("done.")
if boolVerboseMode:
  Out("\n")
else:
  Out("\n")

strImageFileFormat=imgIn.format
if strImageFileFormat.lower()!="gif":
  Fatal("\""+strInFile+"\" isn't a GIF file!")
strImageFileMode=imgIn.mode
if strImageFileMode.lower()!="p":
  Fatal("\""+strInFile+"\" isn't a palette-based file!")

numColorTableByte=ord(arrInFile[10])
if numColorTableByte&128==0:
  Fatal("\""+strInFile+"\" doesn't contain a global color table!")
numImageBitplanes=(numColorTableByte&0b111)+1
numPaletteSize=pow(2, numImageBitplanes)



if boolMaskFileGiven:
  Out("Reading mask file \""+strMaskFile+"\"... ")
  arrMaskFile=xia.GetBinaryFile(strMaskFile)
  try:
    imgMask=Image.open(strMaskFile)
  except IOError:
    Fatal("\""+strMaskFile+"\" isn't a valid image file!")
  Out("done.")
  if boolVerboseMode:
    Out("\n")
  else:
    Out("\n")

  strImageFileFormat=imgMask.format
  if strImageFileFormat.lower()!="gif":
    Fatal("\""+strMaskFile+"\" isn't a GIF file!")
  strImageFileMode=imgMask.mode
  if strImageFileMode.lower()!="p":
    Fatal("\""+strMaskFile+"\" isn't a palette-based file!")

  numColorTableByte=ord(arrMaskFile[10])
  if numColorTableByte&128==0:
    Fatal("\""+strMaskFile+"\" doesn't contain a global color table!")
  numImageBitplanes=(numColorTableByte&0b111)+1
  numPaletteSize=pow(2, numImageBitplanes)



numPalFileOffset=13
arrGifPal=[]
arrAtariPal=[]

for i in range(numPaletteSize):
  thisCol=arrInFile[numPalFileOffset:numPalFileOffset+3]
  #print "thisCol: "+str(thisCol)
  tempCol=copy.deepcopy(thisCol)
  thisCol=[]
  for t in tempCol:
    thisCol.append(ord(t))
  arrGifPal.append(thisCol)
  numPalFileOffset+=3
  arrAtariPal.append(xia.MakeFormattedColor(thisCol, strColorFormat))

for i in range(256-numPaletteSize):
  thisCol=[0xf0, 0x00, 0xf0]
  arrAtariPal.append(xia.MakeFormattedColor(thisCol, strColorFormat))



# Image stuff
arrImage=list(imgIn.getdata()) # one int per pixel, value=palette index, so no conversion necessary!
numImageSize=len(arrImage)

if boolMaskFileGiven:
  arrMask=list(imgMask.getdata()) # one int per pixel, value=palette index, so no conversion necessary!
  numMaskSize=len(arrMask)
else:
  arrMask=[]

# Test image to see it doesn't contain more colors than allowed by --bitplanes
numMaxColor=max(arrImage)
numMaxColorFromBitplanes=pow(2, numBitplanes)
if numMaxColor>numMaxColorFromBitplanes:
  print
  Fatal("--bitplanes set to "+str(numSpriteBitplanes)+" (max "+str(numMaxColorFromBitplanes)+" colors), but the highest\ncolor index in image is "+str(numMaxColor)+"!")


## Reading input data
################################################################################################
################################################################################################
## Process image


arrImageDimensions=imgIn.size
numImageWidth=arrImageDimensions[0]
numImageHeight=arrImageDimensions[1]

boolNoMaskMode=False
if not boolColorMask and not boolMaskFileGiven:
  Notice("No mask supplied, and no auto- or color masking given, so mask will be all 0's!")
  arrMask=[0]*numImageHeight*numImageWidth  # len(arrImage)
  boolNoMaskMode=True


if boolMaskFileGiven:
  arrMaskDimensions=imgMask.size
  numMaskWidth=arrMaskDimensions[0]
  numMaskHeight=arrMaskDimensions[1]
  if (numImageWidth!=numMaskWidth) or (numImageHeight!=numMaskHeight):
    Fatal("Mask file ("+str(numMaskWidth)+"x"+str(numMaskHeight)+") must be the same size as the sprite file ("+str(numImageWidth)+"x"+str(numImageHeight)+")!")

numOriginalSpriteWidth=numImageWidth
numOriginalSpriteHeight=numImageHeight/2

# If image's width isn't divisible by 16
boolWidthIsDivisibleBy16=False

numNewImageWidth=0
numWidthAdjust=0
if numImageWidth%16.0==0:
  boolWidthIsDivisibleBy16=True
  numNewImageWidth=numImageWidth
else:
  numWidthAdjust=16-(numImageWidth%16)
  numNewImageWidth=numImageWidth+numWidthAdjust
  xia.Notice("Width changed from "+str(numImageWidth)+" to "+str(numNewImageWidth)+" to make it word-divisible\n")


# Extend image to width divisible by 16
arrImageTemp=[]
arrMaskTemp=[]
#numMaskHeight=numImageHeight/2
numSourcePos=0
for y in range(numImageHeight):
  for x in range(numImageWidth):
    thisPixel=arrImage[numSourcePos]
    arrImageTemp.append(thisPixel)
    if boolMaskFileGiven:
      thisPixel=arrMask[numSourcePos]
      arrMaskTemp.append(thisPixel)
    numSourcePos+=1
  for x in range(numNewImageWidth-numImageWidth):
    if numColorMask==-1:
      thisPixel=1
    else:
      thisPixel=numColorMask
    arrImageTemp.append(thisPixel)
    if boolMaskFileGiven:
      arrMaskTemp.append(1)


arrImage=copy.deepcopy(arrImageTemp)
arrMask=copy.deepcopy(arrMaskTemp)


numImageSize=len(arrImage)
numImageWidth=numNewImageWidth


if boolVerboseMode:
  Out("\nSprite image data:\n")
  Out("  Bitplanes in sprite: "+str(numBitplanes)+"\n")
  Out("  Bitplanes in image: "+str(numImageBitplanes)+"\n")
  Out("  Pixels in sprite: "+str(numImageSize)+"\n")
  Out("  Sprite size: "+str(numImageWidth)+" x "+str(numImageHeight)+"\n")
  DumpPal()


## Process image
################################################################################################
################################################################################################
## Convert to sprite data


if boolVerboseMode:
  Out("\n\nConverting sprite... ")

arrResult=ConvertImagetoBitplanesAndMask(arrImage, arrMask, numColorMask)
arrAtariSprite=arrResult["sprite"]
arrAtariMask=arrResult["mask"]

# Mask off sprite data
boolMaskOffSprite=True #False
if boolNoMaskMode:
  boolMaskOffSprite=False
if boolMaskOffSprite:
  boolLocalDebug=False
  numPos=0
  for thisMaskWord in arrAtariMask:
    if boolLocalDebug: print "  mask: "+hex(thisMaskWord)
    for i in range(numBitplanes):
      thisData=arrAtariSprite[numPos+i]
      arrAtariSprite[numPos+i]=thisData&(thisMaskWord^0xffff)
      #arrAtariSprite[numPos+i]=thisData&thisMaskWord
      if boolLocalDebug: print "    sprite: "+hex(thisData)+" -> "+hex(arrAtariSprite[numPos+i])
    numPos+=numBitplanes


if boolVerboseMode:
  PreviewWordMask(arrAtariMask)


if not boolInterleaveBitplanes:
  # De-interleave bitplanes in sprite
  arrTemp=copy.deepcopy(arrAtariSprite)
  arrPlanes=[]
  for i in range(numBitplanes):
    thisArr=[]
    arrPlanes.append(thisArr)
  for i in range(len(arrTemp)):
    thisWord=arrTemp[i]
    arrPlanes[i%numBitplanes].append(thisWord)

  arrAtariSprite=[]
  for thisPlane in arrPlanes:
    for thisWord in thisPlane:
      arrAtariSprite.append(thisWord)


# Convert to bytes
arrSpriteBytes=TurnWordsIntoBytes(arrAtariSprite)
arrMaskBytes=TurnWordsIntoBytes(arrAtariMask)


## Convert to sprite data
################################################################################################
################################################################################################
## Write result to disk


strOutStub=strOutFile
if strOutStub[-4:]==".spr":
  strOutStub=strOutStub[:-4]

if boolVerboseMode:
  Out("\n")

strOutFileSprite=strOutStub+".spr"
strOutFileMask=strOutStub+".msk"
Out("Writing file \""+strOutFileSprite+"\" ("+str(len(arrSpriteBytes))+" bytes)... ")
xia.WriteBinaryFile(arrSpriteBytes, strOutFileSprite)
Out("done!\n")
Out("Writing file \""+strOutFileMask+"\" ("+str(len(arrMaskBytes))+" bytes)... ")
xia.WriteBinaryFile(arrMaskBytes, strOutFileMask)
Out("done!\n")



strOutFileSource=strOutStub+".s"
arrSource=[]
#                 1234567890123456789012345678901234567890123456789012345678901234567890
arrSource.append(";#####################################################################")
arrSource.append(";## Sprite data exported using gif2sprite v"+strVersion+" by XiA")
arrSource.append(";## Source file: "+strInFile)
arrSource.append("")
arrSource.append("  dc.w "+str(numBitplanes)+"  ; number of bitplanes")
arrSource.append("  dc.w "+str(numImageWidth)+"  ; width (px)")
arrSource.append("  dc.w "+str(numImageHeight)+"  ; height (px)")
arrSource.append("")
arrSource.append(";## Sprite data exported using gif2sprite v"+strVersion+" by XiA")
arrSource.append(";#####################################################################")
Out("Writing file \""+strOutFileSource+"\"... ")
xia.WriteTextFile(arrSource, strOutFileSource)
Out("done!\n")


if boolSaveMaskAsGif:
  arrPalette=[]
  for i in range(3): arrPalette.append(0)
  for i in range(3): arrPalette.append(255)

  arrMaskChunky=[]
  for b in arrMaskBytes:
    strByte=bin(b)[2:].zfill(8)
    for c in strByte:
      if c=="0":
        arrMaskChunky.append(0)
      else:
        arrMaskChunky.append(1)

  strFilename=strOutFileMask+".gif"
  Out("Writing file \""+strFilename+"\"... ")
  imgOut=Image.new("P", (numImageWidth, numImageHeight))
  imgOut.putdata(arrMaskChunky)
  imgOut.save(strFilename, "gif", palette=arrPalette)
  Out("done!\n")


## Write result to disk
################################################################################################
################################################################################################
