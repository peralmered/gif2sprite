# gif2sprite

Converts gif files to sprites for Atari 16/32-bit computers' bitplane graphics modes.

Intended usage is blitting, so this tool doesn't add 16px on one side, and neither does it do pre-shifting.


## Requirements

* Python v2.7.x

* Pillow (PIL fork)

    pip install pillow

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


## Usage

    python gif2sprite -i *mysprite.gif* -b *number_of_bitplanes* [other options]


### All switches

#### -i (or --infile) *input_file* **required**

Input .gif file

#### -m (or --maskfile) *mask_file*

Mask .gif file

#### -o [or --outfile] *output_file*

Expects a filename without extension; will add extensions automatically

#### -mg [or --savemaskgif]

Export resulting mask to .gif

#### -b [or --bitplanes] *number_of_bitplanes*

Number of bitplanes, 1-8 (defaults to 4 if no value given)

#### -c [or --color-format] *color_format*

"ste", "tt" or "falcon" (default is "ste")

#### -am [or --automask]

Uses color 0 in sprite as mask

#### -cm [or --colormask] *color_index*

Uses color *color_index* in sprite as mask

#### -f [or --feather]

Adds a 1-pixel border around the mask (only works for generated masks, not when you load a mask .gif file)

#### -ib [or --interleavedbitplanes]

Outputs bitplanes interleaved


### Examples:

    python gif2sprite -i ball.gif -b 4 -am

Opens "ball.gif" as sprite data.
Saves "ball.gif.spr" as a 4-bitplane (non-interleaved) sprite, using color 0 as mask.
Saves "ball.gif.msk" as a 1-bitplane mask.

    python gif2sprite -i ghost.gif -b 2 -cm 3 -c tt -ib

Opens "ghost.gif" as sprite data.
Saves "ghost.gif.spr" as a 2-bitplane (interleaved) sprite, using color 3 as mask, Atari TT color format.
Saves "ghost.gif.msk" as a 1-bitplane mask.

    python gif2sprite -i kitten.gif -m kittenmask.gif -o meow -b 3

Opens "kitten.gif" as sprite data, and "kittenmask.gif" as mask data.
Saves "meow.spr" as a 3-bitplanes (non-interleaved) sprite, using "kittenmask.gif" as mask
Saves "meow.msk" as a 1-bitplane mask.
